echo 'getting kubernetes tools'

apk add curl wget

echo 'getting kubectl'

curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

rm kubectl

echo 'getting helm'


wget https://get.helm.sh/helm-v3.6.2-linux-amd64.tar.gz
tar -zxvf helm-v3.6.2-linux-amd64.tar.gz
rm helm-v3.6.2-linux-amd64.tar.gz
mv linux-amd64/helm /usr/local/bin/helm
rm -r linux-amd64

echo 'getting rce'


wget https://github.com/rancher/rke/releases/download/v1.2.9/rke_linux-amd64

chmod +x rke_linux-amd64

mv rke_linux-amd64 /usr/local/bin/rke


