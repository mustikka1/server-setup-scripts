echo 'starting installing docker'
echo 'adding community to repo'
echo 'http://dl-cdn.alpinelinux.org/alpine/latest-stable/community' >> /etc/apk/repositories
apk update
apk add docker docker-compose
echo 'add docker to boot and start'
rc-update add docker boot
service docker start
echo 'docker installed'
